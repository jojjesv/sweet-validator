# Sweet validator for Koa and Express

##  Why?
Validation is done in mass quantities and it would be sweet to minify the code reponsible for validation alone.

##  Validating a request
Use `validateParams`, or `validateBody` to validate a request. You can pass a property string, which might look like this:

`id=oid,name=s[5;10]`

Which translates to: *validate an ObjectID property `id` and a string `name` between 5 and 10 characters.*

The property string's syntax is:
`<name>=<type>[<min>,<max>], ...`.

Properties can also be nested like this:
`<obj>.<name>= ...`.

And if you prefer an object you may pass one. This is the previous string's counterpart:
`{ 'id': 'oid', 'name': 's[5;10]' }`.

### Property string types
The following types are available:
* `s` any string.
  * Use `min` and `max` to validate the string length.
* `b` a boolean.
* `n` a number.
  * `min` and `max` compatible.
* `i` an integer number.
* `o` an object.
* `oid` an ObjectID.
* `date` an ISO date string.
  * `d` also works.
  * `fd` or `fdate` for a *future* date.
  * `pd` or `pdate` for a *past* date.
* `@` an e-mail address.
* `@-` a *deep* e-mail address, which also checks DNS records.
* `u` an absolute URL.
* `du` a data URI, such as an image.
* `p` a phone number of any locale.
  * `swp` a Swedish phone number.
* `fn` a full name of any locale.

### Arrays
You can validate an array by using the following syntax:
`<name>=~<type1><size1>|<type2><size2> ...`.

Means that `<name>` must a an array with values which must confirm to either `<type1><size1>` or `<type2><size2>` etc.

For example: `ages=~n|s[1;2]`.

Or, using objects:

`{ 'ages': ['n', 's[1;2]'] }`.

To ensure an array of an object schema:

`{ 'users': [{ 'name': 's', password: 's[4;25]' }] }`

### Objects
You can validate an object by using the following syntax:
`<parent obj>.<name1>=<type1><size1>,<parent obj>.<name1>=<type1><size1> ...`.

To avoid repeating `<parent obj>` however, you can pass a `$` to reference the previous parent object.

For example, the following are identical:
`update.name=s,$.age=n`

and

`update.name=s,update.age=n`.

The repetition character also works with objects:

`{ 'update.name': 's', '$.age': 'n' }`,

or simply:

`{ 'update': { 'name': 's', 'age': 'n' } }`.

### Parameter comparison
To validate that one parameter is either less than or greater than another parameter, use the following syntax:

*(Both parameters must be of the same type. Works with numbers and dates.)*

`a=i&<b` (`a` should be an integer (`i`), and (`&`), less than (`<`) `b`.)

or, with the greater than operator:

`to=date&>from`

### Optionals
If a property is optional, but still requires validation if passed, append a `?` to the path, like so:
`name?=s[2;25]`.

####  Optional objects
Let's say a `user` property is optional, however if passed, it *must* be an object and *must* conform to:
`{ 'name': 's', 'age': 'd' }`.

We'd use the following syntax for the optional object:

`user?.name=s,$.age=d`

or

`{ 'user?': { 'name': 's', 'age': 'd' } }`.

### Enums
To restrict which values are allows for a property, invoke `registerValidatorEnum` with a given name and restrictive values.

You can reference the enum by its name just like you would with any other type.

For example:

`registerValidatorEnum('allowedAges', [20, 25, 30])`

and then

`age=allowedAges`

or ...

`{ 'age': 'allowedAges' }`

##  Terminology
* A **path** is the path of the property from the request's body, or params object, e.g. `id` or `update.name`.
* A **type** is the type of validator, e.g. `boolean` or `number`.
* A **size** contains the *min* and *max* metadata for a validator, e.g. `[5;25]`.
* A **rule** encapsulates a **type** and a **size**, e.g. `s[5;25]`.
* A **property string** is a string containing a *path* and a *rule*, e.g. `name=s[5;25]`.
* A **property map** is a list of joined *property strings*, e.g. `name=s[5;25],id=oid`.

##  Troubleshooting
### `Type 'Request' is not generic.`
Run `npm i -D @types/express` to install typing dependencies.