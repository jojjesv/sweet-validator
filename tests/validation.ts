import chai, { expect } from 'chai';
import { parseValidators, validateObject } from '../src/index';
import { ObjectID } from 'bson';
import { registerValidatorEnum } from '../src/enum_validator';

describe('validators', () => {
  const validators = parseValidators(null, {
    'id': 'oid',
    'update?.name': 's[5;25]',
    'flag?': 'b',
    'age': 'n[15;40]',
    'date': 'd',
    'email': '@',
    'logoUrl': 'u',
    'imageUri': 'du',
    'preferences?': ['s'],
    'signee': 'fn'
  });

  const valuesToValidate = {
    'id': '5e05086e4f5e2ca248e9ba80',
    'update': {
      'name': 'Eliptis'
    },
    'flag': true,
    'age': 25,
    'date': '2019-12-26T19:21:45.408Z',
    'email': 'johan@eliptis.se',
    'logoUrl': 'https://eliptis-cdn.s3.eu-north-1.amazonaws.com/logo/logo_color_to_black.png',
    'imageUri': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAA==',
    'preferences': ['blue', 'apple', 'hot dogs'],
    'signee': 'John Doe'
  };

  it(`should validate using all available validators`, async () => {
    const result = await validateObject(valuesToValidate, validators);

    expect(result).to.be.true;
  })

  it(`should validate optional property`, async () => {
    const result = await validateObject({
      ...valuesToValidate,
      flag: null
    }, validators);

    expect(result).to.be.true;
  })

  it(`should validate optional array`, async () => {
    const result = await validateObject({
      ...valuesToValidate,
      preferences: null
    }, validators);

    expect(result).to.be.true;
  })

  it(`should validate an optional object property`, async () => {
    const result = await validateObject({
      ...valuesToValidate,
      update: null
    }, validators);

    expect(result).to.be.true;
  })

  it(`should validate an optional nested object property (exists)`, async () => {
    const result = await validateObject({
      company: { name: 'Eliptis AB', logo: { uri: 'https://eliptis-cdn.s3.eu-north-1.amazonaws.com/logo/logo_color_to_black.png' } }
    }, parseValidators(null, {
      'company': { 'name': 's', 'logo?': { 'uri': 'u' } }
    }));

    expect(result).to.be.true;
  });

  it(`should validate an optional nested object property (null)`, async () => {
    const result = await validateObject({
      company: { name: 'Eliptis AB', logo: null }
    }, parseValidators(null, {
      'company': { 'name': 's', 'logo?': { 'uri': 'u' } }
    }));

    expect(result).to.be.true;
  });

  it('should validate enum values', async () => {
    registerValidatorEnum('allowedAges', [15, 25, 35]);

    const result = await validateObject({
      age: 15
    }, parseValidators(null, 'age=allowedAges'));

    expect(result).to.be.true;
  });

  it('should validate enum values with alternative syntax', async () => {
    const allowedValues = ['a', 'b', 'c'];
    registerValidatorEnum({ allowedValues });

    const result = await validateObject({
      value: 'a'
    }, parseValidators(null, 'value=allowedValues'));

    expect(result).to.be.true;
  });

  it('should validate array with object schema', async () => {
    const result = await validateObject({
      users: [
        { name: 'johnson', password: 'qwerty' },
        { name: 'micheal', password: 'password1' }
      ]
    }, parseValidators(null, {
      'users': [{
        'name': 's',
        'password': 's[4;20]'
      }]
    }));

    expect(result).to.be.true;
  });

  it('should deep validate email', async () => {
    const result = await validateObject({
      email: 'johan@eliptis.se'
    }, parseValidators(null, 'email=@-'));

    expect(result).to.be.true;
  }).timeout(12500);

  it('should validate an integer', async () => {
    const result = await validateObject({
      age: 35
    }, parseValidators(null, 'age=i[25;]'));

    expect(result).to.be.true;
  });

  it('should validate numeric without max', async () => {
    const result = await validateObject({
      age: 35
    }, parseValidators(null, 'age=n[25;]'));

    expect(result).to.be.true;
  });

  it('should validate string without max', async () => {
    const result = await validateObject({
      name: 'alice'
    }, parseValidators(null, 'name=s[2;]'));

    expect(result).to.be.true;
  });

  it('should validate numeric string', async () => {
    const result = await validateObject({
      age: '25'
    }, parseValidators({
      validateNumericStrings: true
    }, 'age=i[18;]'));

    expect(result).to.be.true;
  });

  it('should validate a future date', async () => {
    const result = await validateObject({
      deadline: new Date(Date.now() + 36e3).toISOString()
    }, parseValidators(null, 'deadline=fd'));

    expect(result).to.be.true;
  });

  it('should validate a past date', async () => {
    const result = await validateObject({
      dateOfBirth: new Date(Date.now() - 36e3).toISOString()
    }, parseValidators(null, 'dateOfBirth=pd'));

    expect(result).to.be.true;
  });

  it('should validate a date by less-than comparison', async () => {
    const result = await validateObject({
      from: new Date('2020-01-01T01:00:00.000Z').toISOString(),
      to: new Date('2020-01-01T03:00:00.000Z').toISOString()
    }, parseValidators(null, 'from=d&<to', 'to=d'));

    expect(result).to.be.true;
  });

  it('should validate a date by greater-than comparison', async () => {
    const result = await validateObject({
      from: new Date('2020-01-01T01:00:00.000Z').toISOString(),
      to: new Date('2020-01-01T03:00:00.000Z').toISOString()
    }, parseValidators(null, 'to=d&>from', 'from=d'));

    expect(result).to.be.true;
  });

  it('should validate a numeric by less-than comparison', async () => {
    const result = await validateObject({
      age: 12,
      maxAge: 18
    }, parseValidators(null, 'age=i&<maxAge', 'maxAge=i'));

    expect(result).to.be.true;
  });

  it('should validate a numeric by greater-than comparison', async () => {
    const result = await validateObject({
      age: 24,
      minAge: 18
    }, parseValidators(null, 'age=n&>minAge', 'minAge=n'));

    expect(result).to.be.true;
  });

  it('should validate a phone number (any locale)', async () => {
    const result = await validateObject({
      phone: '703-354-8329',
    }, parseValidators(null, 'phone=p'));

    expect(result).to.be.true;
  });

  it('should validate a phone number (Swedish)', async () => {
    const result = await validateObject({
      phone: '0735-557123',
    }, parseValidators(null, 'phone=swp'));

    expect(result).to.be.true;
  });

  it('should validate a full name', async () => {
    const result = await validateObject({
      name: 'John Doè',
    }, parseValidators(null, 'name=fn'));

    expect(result).to.be.true;
  });
});