describe('sweet validator', () => {
  require('./parsing');
  require('./validation');
  require('./invalidation');
});