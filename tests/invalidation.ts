import chai, { expect } from 'chai';
import { parseValidators, validateObject } from '../src/index';
import { registerValidatorEnum } from '../src/enum_validator';

describe('invalidation', () => {
  const validators = parseValidators(null, {
    'id': 'oid',
    'update?.name': 's[5;25]',
    'flag?': 'b',
    'height': 'n[50;200]',
    'age': 'i[15;40]',
    'date': 'd',
    'email': '@',
    'logoUrl': 'u',
    'imageUri': 'du'
  });

  const validObject = {
    'id': '5e05086e4f5e2ca248e9ba80',
    'update': {
      'name': 'Eliptis'
    },
    'flag': true,
    'height': 175.5,
    'age': 25,
    'date': '2019-12-26T19:21:45.408Z',
    'email': 'johan@eliptis.se',
    'logoUrl': 'https://eliptis-cdn.s3.eu-north-1.amazonaws.com/logo/logo_color_to_black.png',
    'imageUri': 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAA=='
  };

  it(`should invalidate missing prop`, async () => {
    const result = await validateObject({
      ...validObject,
      email: undefined
    }, validators);

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`email`');
  });

  it(`should invalidate missing array`, async () => {
    const result = await validateObject({
      users: undefined
    }, parseValidators(null, {
      'users': [{
        'name': 's',
        'password': 's[4;20]'
      }]
    }));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`users`');
  });

  it(`should invalidate missing enum prop`, async () => {
    registerValidatorEnum('allowedAges', [15, 25, 35]);

    const result = await validateObject({
      age: undefined
    }, parseValidators(null, 'age=allowedAges'));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`age`');
  });

  it(`should invalidate incorrect type (email / string)`, async () => {
    const result = await validateObject({
      ...validObject,
      email: 'notanemail.bro'
    }, validators);

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`email`');
  });

  it(`should invalidate incorrect optional object property`, async () => {
    const result = await validateObject({
      ...validObject,
      update: 'notAnObject'
    }, validators);

    expect(result).to.be.an('array').with.lengthOf(2);
    expect(result[0]).to.contain('`update`');
    expect(result[1]).to.contain('`update.name`');
  })

  it(`should invalidate length of string (max)`, async () => {
    const result = await validateObject({
      ...validObject,
      update: {
        name: 'A very long name indeed and beyond 25 characters'
      }
    }, validators);

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`update.name`');
  });

  it(`should invalidate length of string (min)`, async () => {
    const result = await validateObject({
      ...validObject,
      update: {
        name: 'Ye'
      }
    }, validators);

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`update.name`');
  });

  it(`should invalidate an integer`, async () => {
    const result = await validateObject({
      ...validObject,
      age: 16.5
    }, validators);

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`age`');
  });


  it(`should invalidate size of number (max)`, async () => {
    const result = await validateObject({
      ...validObject,
      height: 2
    }, validators);

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`height`');
  });

  it(`should invalidate size of number (min)`, async () => {
    const result = await validateObject({
      ...validObject,
      height: 25
    }, validators);

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`height`');
  });

  it('should invalidate an enum', async () => {
    registerValidatorEnum('allowedAges', [15, 25, 35]);

    const result = await validateObject({
      age: 16
    }, parseValidators(null, 'age=allowedAges'));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`age`');
  });

  it('should invalidate array with object schema', async () => {
    const result = await validateObject({
      users: [
        { name: null, password: 'qwerty' },
        { name: 'micheal', password: 5 }
      ]
    }, parseValidators(null, {
      'users': [{
        'name': 's',
        'password': 's[4;20]'
      }]
    }));

    expect(result).to.be.an('array').with.lengthOf(2);
    expect(result[0]).to.contain('`name`');
    expect(result[1]).to.contain('`password`');
  });

  it('should invalidate non-array property', async () => {
    const result = await validateObject({
      users: false
    }, parseValidators(null, {
      'users': [{
        'name': 's',
        'password': 's[4;20]'
      }]
    }));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`users`');
  });

  it('should deep invalidate email', async () => {
    const result = await validateObject({
      email: `doesabsolutelynotexistnoway@bad-domain${Math.random().toString(16)}.com`
    }, parseValidators(null, 'email=@-'));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`email`');
  });

  it('should invalidate optional object with invalid inner property type', async () => {
    const result = await validateObject({
      user: { name: 100, age: 26 }
    }, parseValidators(null, {
      'user?': { 'name': 's', 'age': 'n' }
    }));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`user.name`');
  });

  it('should invalidate optional object with missing inner property', async () => {
    const result = await validateObject({
      user: { age: 26 }
    }, parseValidators(null, {
      'user?': { 'name': 's', 'age': 'n' }
    }));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`user.name`');
  });

  it('should invalidate optional nested object', async () => {
    const result = await validateObject({
      company: { name: 'Eliptis AB', logo: false }
    }, parseValidators(null, {
      'company': { 'name': 's', 'logo?': { 'uri': 'u' } }
    }));

    expect(result).to.be.an('array').with.lengthOf(2);
    expect(result[0]).to.contain('`company.logo`');
    expect(result[1]).to.contain('`company.logo.uri`');
  });

  it('should invalidate optional nested object property', async () => {
    const result = await validateObject({
      company: { name: 'Eliptis AB', logo: { uri: false } }
    }, parseValidators(null, {
      'company': { 'name': 's', 'logo?': { 'uri': 'u' } }
    }));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`company.logo.uri`');
  });

  it('should invalidate absolute URL', async () => {
    const result = await validateObject({
      uri: '/not/an/absoluteUrl'
    }, parseValidators(null, {
      'uri': 'u'
    }));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`uri`');
  });

  it('should invalidate data URI', async () => {
    const result = await validateObject({
      uri: 'adata:image\png;base64,iVBORw0KGgoAAAANSUhEUgAA=='
    }, parseValidators(null, {
      'uri': 'du'
    }));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`uri`');
  });

  it('should invalidate string without max', async () => {
    const result = await validateObject({
      name: 'ali'
    }, parseValidators(null, 'name=s[5;]'));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`name`');
  });

  it('should invalidate a past date', async () => {
    const result = await validateObject({
      deadline: new Date(Date.now() - 36e3).toISOString()
    }, parseValidators(null, 'deadline=fd'));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`deadline`');
  });

  it('should validate a future date', async () => {
    const result = await validateObject({
      dateOfBirth: new Date(Date.now() + 36e3).toISOString()
    }, parseValidators(null, 'dateOfBirth=pd'));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.contain('`dateOfBirth`');
  });

  it('should invalidate a date by less-than comparison', async () => {
    const result = await validateObject({
      from: new Date('2020-01-01T02:00:00.000Z').toISOString(),
      to: new Date('2020-01-01T01:00:00.000Z').toISOString()
    }, parseValidators(null, 'from=d&<to', 'to=d'));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.eq('`from` must be before `to`');
  });

  it('should invalidate a date by greater-than comparison', async () => {
    const result = await validateObject({
      from: new Date('2020-01-01T02:00:00.000Z').toISOString(),
      to: new Date('2020-01-01T01:00:00.000Z').toISOString()
    }, parseValidators(null, 'to=d&>from', 'from=d'));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).to.eq('`to` must be after `from`');
  });

  it('should invalidate a numeric by less-than comparison', async () => {
    const result = await validateObject({
      age: 36,
      maxAge: 18
    }, parseValidators(null, 'age=i&<maxAge', 'maxAge=i'));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).eq('`age` must be less than `maxAge`');
  });

  it('should invalidate a numeric by greater-than comparison', async () => {
    const result = await validateObject({
      age: 12,
      minAge: 18
    }, parseValidators(null, 'age=n&>minAge', 'minAge=n'));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).eq('`age` must be greater than `minAge`');
  });

  it('should invalidate referenced type', async () => {
    const result = await validateObject({
      age: 12,
      minAge: new Date().toISOString()
    }, parseValidators(null, 'age=n&>minAge', 'minAge=d'));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).eq('`minAge` must be numeric as it is referenced by `age`');
  });

  it('should invalidate a phone number (any locale)', async () => {
    const result = await validateObject({
      phone: '+39-346-5559-aa',
    }, parseValidators(null, 'phone=p'));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).eq('`phone` must be a valid phone number');
  });

  it('should validate a phone number (Swedish)', async () => {
    const result = await validateObject({
      phone: '+46-735-5512aa',
    }, parseValidators(null, 'phone=swp'));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).eq('`phone` must be a valid Swedish phone number');
  });

  it('should invalidate missing email', async () => {
    const result = await validateObject({
      email: undefined
    }, parseValidators(null, 'email=@'));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).eq('`email` must be a valid e-mail address');
  });

  it('should invalidate full name', async () => {
    const result = await validateObject({
      name: 'Will+iam'
    }, parseValidators(null, 'name=fn'));

    expect(result).to.be.an('array').with.lengthOf(1);
    expect(result[0]).eq('`name` must be a valid full name');
  });
});