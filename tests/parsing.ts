import chai, { expect } from 'chai';
import { parseValidators, validateObject } from '../src/index';
import { ObjectID } from 'bson';
import { registerValidatorEnum } from '../src/enum_validator';

describe('validation syntax', () => {
  describe('validation property string', () => {
    it('should parse a validation property string', () => {
      const parsed = parseValidators(null,
        'id=oid,update.name=s[5;25],flag?=b,age=n[15;40],dateOfBirth=d'
      );

      expect(parsed).to.have.lengthOf(5);
      expect(parsed[0]).to.have.property('path', 'id');
      expect(parsed[0]).to.have.property('type', 'objectid');
      expect(parsed[0]).to.have.property('min', null);
      expect(parsed[0]).to.have.property('max', null);
      expect(parsed[0]).to.have.property('optional', false);

      expect(parsed[1]).to.have.property('path', 'update.name');
      expect(parsed[1]).to.have.property('type', 'string');
      expect(parsed[1]).to.have.property('min', 5);
      expect(parsed[1]).to.have.property('max', 25);

      expect(parsed[2]).to.have.property('path', 'flag');
      expect(parsed[2]).to.have.property('type', 'boolean');
      expect(parsed[2]).to.have.property('optional', true);

      expect(parsed[3]).to.have.property('path', 'age');
      expect(parsed[3]).to.have.property('type', 'number');
      expect(parsed[3]).to.have.property('min', 15);
      expect(parsed[3]).to.have.property('max', 40);

      expect(parsed[4]).to.have.property('path', 'dateOfBirth');
      expect(parsed[4]).to.have.property('type', 'date');
    })

    it('should parse a spread validation property string', () => {
      const parsed = parseValidators(null,
        'id=oid', 'update.name=s[5;25]', 'flag?=b', 'age=n[15;40]', 'dateOfBirth=d'
      );

      expect(parsed).to.have.lengthOf(5);
      expect(parsed[0]).to.have.property('path', 'id');
      expect(parsed[0]).to.have.property('type', 'objectid');
      expect(parsed[0]).to.have.property('min', null);
      expect(parsed[0]).to.have.property('max', null);
      expect(parsed[0]).to.have.property('optional', false);

      expect(parsed[1]).to.have.property('path', 'update.name');
      expect(parsed[1]).to.have.property('type', 'string');
      expect(parsed[1]).to.have.property('min', 5);
      expect(parsed[1]).to.have.property('max', 25);

      expect(parsed[2]).to.have.property('path', 'flag');
      expect(parsed[2]).to.have.property('type', 'boolean');
      expect(parsed[2]).to.have.property('optional', true);

      expect(parsed[3]).to.have.property('path', 'age');
      expect(parsed[3]).to.have.property('type', 'number');
      expect(parsed[3]).to.have.property('min', 15);
      expect(parsed[3]).to.have.property('max', 40);

      expect(parsed[4]).to.have.property('path', 'dateOfBirth');
      expect(parsed[4]).to.have.property('type', 'date');
    });

    it('should parse an array property string', () => {
      const parsed = parseValidators(null,
        'ages=~n|s[1;2]'
      );

      expect(parsed).to.have.lengthOf(1);
      expect(parsed[0]).to.have.property('path', 'ages');
      expect(parsed[0]).property('type').to.be.an('array').with.lengthOf(2);

      expect(parsed[0].type[0]).to.have.property('type', 'number');

      expect(parsed[0].type[1]).to.have.property('type', 'string');
      expect(parsed[0].type[1]).to.have.property('min', 1);
      expect(parsed[0].type[1]).to.have.property('max', 2);
    });

    it('should parse path placeholders', () => {
      const parsed = parseValidators(null,
        'update.name=s,$.age=n[15;25],$.dateOfBirth=d'
      );

      expect(parsed).to.have.lengthOf(3);

      expect(parsed[0]).to.have.property('path', 'update.name');
      expect(parsed[0]).to.have.property('type', 'string');

      expect(parsed[1]).to.have.property('path', 'update.age');
      expect(parsed[1]).to.have.property('type', 'number');

      expect(parsed[2]).to.have.property('path', 'update.dateOfBirth');
      expect(parsed[2]).to.have.property('type', 'date');
    });

    it('should parse an optional object', () => {
      const parsed = parseValidators(null,
        'user?.name=s,$.age=n[15;25]'
      );

      expect(parsed).to.have.lengthOf(3);
      expect(parsed[0]).to.have.property('path', 'user');
      expect(parsed[0]).to.have.property('type', 'object');
      expect(parsed[0]).to.have.property('optional', true);

      expect(parsed[1]).to.have.property('path', 'user.name');
      expect(parsed[1]).to.have.property('type', 'string');
      expect(parsed[1]).to.have.property('optional', false);

      expect(parsed[2]).to.have.property('path', 'user.age');
      expect(parsed[2]).to.have.property('type', 'number');
      expect(parsed[2]).to.have.property('optional', false);
    });
  });

  describe('validation property map object', () => {
    it('should parse a property object', () => {
      const parsed = parseValidators(null, {
        'id': 'oid',
        'update.name': 's[5;25]',
        'flag': 'b',
        'age': 'n[15;40]',
        'dateOfBirth': 'd'
      });

      expect(parsed).to.have.lengthOf(5);
      expect(parsed[0]).to.have.property('path', 'id');
      expect(parsed[0]).to.have.property('type', 'objectid');
      expect(parsed[0]).to.have.property('min', null);
      expect(parsed[0]).to.have.property('max', null);

      expect(parsed[1]).to.have.property('path', 'update.name');
      expect(parsed[1]).to.have.property('type', 'string');
      expect(parsed[1]).to.have.property('min', 5);
      expect(parsed[1]).to.have.property('max', 25);

      expect(parsed[2]).to.have.property('path', 'flag');
      expect(parsed[2]).to.have.property('type', 'boolean');

      expect(parsed[3]).to.have.property('path', 'age');
      expect(parsed[3]).to.have.property('type', 'number');
      expect(parsed[3]).to.have.property('min', 15);
      expect(parsed[3]).to.have.property('max', 40);

      expect(parsed[4]).to.have.property('path', 'dateOfBirth');
      expect(parsed[4]).to.have.property('type', 'date');
    });

    it('should parse path placeholders', () => {
      const parsed = parseValidators(null, {
        'update.name': 's[5;25]',
        '$.age': 'n',
        '$.dateOfBirth': 'd'
      });

      expect(parsed).to.have.lengthOf(3);

      expect(parsed[0]).to.have.property('path', 'update.name');
      expect(parsed[0]).to.have.property('type', 'string');

      expect(parsed[1]).to.have.property('path', 'update.age');
      expect(parsed[1]).to.have.property('type', 'number');

      expect(parsed[2]).to.have.property('path', 'update.dateOfBirth');
      expect(parsed[2]).to.have.property('type', 'date');
    });

    it('should parse nested objects', () => {
      const parsed = parseValidators(null, {
        'update': {
          'name': 's[5;25]',
          'age': 'n',
          'dateOfBirth': 'd',
          'address': {
            'street': 's',
            'zip': 's'
          }
        }
      });

      expect(parsed).to.have.lengthOf(5);

      expect(parsed[0]).to.have.property('path', 'update.name');
      expect(parsed[0]).to.have.property('type', 'string');
      expect(parsed[0]).to.have.property('min', 5);
      expect(parsed[0]).to.have.property('max', 25);

      expect(parsed[1]).to.have.property('path', 'update.age');
      expect(parsed[1]).to.have.property('type', 'number');

      expect(parsed[2]).to.have.property('path', 'update.dateOfBirth');
      expect(parsed[2]).to.have.property('type', 'date');

      expect(parsed[3]).to.have.property('path', 'update.address.street');
      expect(parsed[3]).to.have.property('type', 'string');

      expect(parsed[4]).to.have.property('path', 'update.address.zip');
      expect(parsed[4]).to.have.property('type', 'string');
    });

    it('should parse an array property with object schema', () => {
      const parsed = parseValidators(null, {
        'users': [{ 'name': 's', 'password': 's[4;24]' }]
      });

      expect(parsed).to.have.lengthOf(1);
      expect(parsed[0]).to.have.property('path', 'users');
      expect(parsed[0]).property('type').to.be.an('array').with.lengthOf(2);

      expect(parsed[0].type[0]).to.have.property('path', 'name');
      expect(parsed[0].type[0]).to.have.property('type', 'string');

      expect(parsed[0].type[1]).to.have.property('path', 'password');
      expect(parsed[0].type[1]).to.have.property('type', 'string');
      expect(parsed[0].type[1]).to.have.property('min', 4);
      expect(parsed[0].type[1]).to.have.property('max', 24);
    });

    it('should parse an optional object', () => {
      const parsed = parseValidators(null, {
        'user?': { 'name': 's', 'age': 'n' }
      });

      expect(parsed).to.have.lengthOf(3);
      expect(parsed[0]).to.have.property('path', 'user');
      expect(parsed[0]).to.have.property('type', 'object');
      expect(parsed[0]).to.have.property('optional', true);

      expect(parsed[1]).to.have.property('path', 'user.name');
      expect(parsed[1]).to.have.property('type', 'string');
      expect(parsed[1]).to.have.property('optional', false);

      expect(parsed[2]).to.have.property('path', 'user.age');
      expect(parsed[2]).to.have.property('type', 'number');
      expect(parsed[2]).to.have.property('optional', false);
    });

    it('should parse an array with element min / max', () => {
      const parsed = parseValidators(null, {
        'preferences': ['s[10;20]']
      });

      expect(parsed).to.have.lengthOf(1);
      expect(parsed[0]).to.have.property('path', 'preferences');
      expect(parsed[0]).property('type').to.be.an('array');
      expect(parsed[0]).to.have.property('optional', false);
      expect(parsed[0].type[0]).to.have.property('min', 10);
      expect(parsed[0].type[0]).to.have.property('max', 20);
    });

    it('should parse an optional array', () => {
      const parsed = parseValidators(null, {
        'preferences?': ['s']
      });

      expect(parsed).to.have.lengthOf(1);
      expect(parsed[0]).to.have.property('path', 'preferences');
      expect(parsed[0]).property('type').to.be.an('array');
      expect(parsed[0]).to.have.property('optional', true);
    });

    it('should parse an optional nested object', () => {
      const parsed = parseValidators(null, {
        'company': { 'name': 's', 'logo?': { uri: 'u' } }
      });

      expect(parsed).to.have.lengthOf(3);

      expect(parsed[0]).to.have.property('path', 'company.name');
      expect(parsed[0]).to.have.property('type', 'string');

      expect(parsed[1]).to.have.property('path', 'company.logo');
      expect(parsed[1]).to.have.property('type', 'object');
      expect(parsed[1]).to.have.property('optional', true);

      expect(parsed[2]).to.have.property('path', 'company.logo.uri');
      expect(parsed[2]).to.have.property('type', 'absoluteurl');
      expect(parsed[2]).to.have.property('optional', false);
    });

    it('should parse min without max', () => {
      const parsed = parseValidators(null, 'name=s[2;]');

      expect(parsed).to.have.lengthOf(1);
      expect(parsed[0]).to.have.property('path', 'name');
      expect(parsed[0]).to.have.property('type', 'string');
      expect(parsed[0]).to.have.property('optional', false);
      expect(parsed[0]).to.have.property('min', 2);
      expect(parsed[0]).to.have.property('max').to.be.null;
    });
  });

  describe('validation comparison', () => {
    it('should parse a less than operator', () => {
      const parsed = parseValidators(null, 'age=i&<minAge', 'minAge=i');

      expect(parsed).to.have.lengthOf(2);
      expect(parsed[0]).to.have.property('path', 'age');
      expect(parsed[0]).to.have.property('type', 'integer');
      expect(parsed[0]).to.have.property('optional', false);
      expect(parsed[0]).property('compareWith').to.eql({ path: 'minAge', operator: 'lessThan' });
    });

    it('should parse a greater than operator', () => {
      const parsed = parseValidators(null, 'age=i&>minAge', 'minAge=i');

      expect(parsed).to.have.lengthOf(2);
      expect(parsed[0]).to.have.property('path', 'age');
      expect(parsed[0]).to.have.property('type', 'integer');
      expect(parsed[0]).to.have.property('optional', false);
      expect(parsed[0]).property('compareWith').to.eql({ path: 'minAge', operator: 'greaterThan' });
    });

    it('should ignore comparison if missing path', () => {
      const parsed = parseValidators(null, 'age=i&<');

      expect(parsed).to.have.lengthOf(1);
      expect(parsed[0]).property('compareWith').to.be.null;
    });
  });
});