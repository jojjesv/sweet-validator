import { DefaultContext, ParameterizedContext } from "koa";
import { ValidationResult } from "./validators";
import { Request, Response, NextFunction } from "express";
export { registerValidatorEnum } from './enum_validator';
declare type PropertyStringMap = string;
export declare class ValidationOpts {
    /**
     * Whether to validate numeric-like strings as numerics, such as `"42"`.
     * @default false
     */
    validateNumericStrings?: boolean;
    /**
     * Whether to validate boolean-like strings as boolean, such as `"false"`.
     * @default false
     */
    validateBooleanStrings?: boolean;
}
declare class ValidatorRule extends ValidationOpts {
    type: ValidatorType | ValidatorRule[];
    min?: number;
    max?: number;
    compareWith?: {
        operator: ComparisonValidationOperator;
        path: string;
    };
}
export declare class Validator extends ValidatorRule {
    optional: boolean;
    path: string;
    parentObjectIsOptional?: boolean;
}
export declare type ComparisonValidationOperator = 'lessThan' | 'greaterThan';
declare type ValidationArgument = PropertyStringMap | {
    [propName: string]: ValidationArgument | ValidationArgument[];
};
export declare type ValidatorType = 'string' | 'boolean' | 'number' | 'integer' | 'object' | 'objectid' | 'date' | 'datefuture' | 'datepast' | 'email' | 'emaildeep' | 'absoluteurl' | 'datauri' | 'phone' | 'swedishphone' | 'fullname';
export declare function parseValidators(opts: ValidationOpts, ...args: ValidationArgument[]): Validator[];
export declare function parsePropertyMap(str: PropertyStringMap): Validator[];
/**
 * Validates an object using validators.
 */
export declare function validateObject(obj: any, validators: Validator[]): Promise<true | ValidationResult[]>;
export declare const koa: {
    /**
     * Validate a koa request using your own selector.
     *
     * If you are passing a multipart form but would prefer to a JSON body,
     * use thie function to select and validate the JSON body part.
     *
     * @param selector Selects object to validate
     * @param args Validation arguments
     * @returns Validation middleware
     */
    validate: (selector: (context: ParameterizedContext<import("koa").DefaultState, DefaultContext>) => any, opts: ValidationOpts, ...args: ValidationArgument[]) => any;
    validateBody: (...args: ValidationArgument[]) => any;
    validateParams: (...args: ValidationArgument[]) => any;
    validateQuery: (...args: ValidationArgument[]) => any;
    /**
     * Validates `limit`, and `offset` query args.
     */
    validateQueryPaging: () => any;
};
export declare const express: {
    validate: (selector: (req: Request<import("express-serve-static-core").ParamsDictionary>, res: Response) => any, opts: ValidationOpts, ...args: ValidationArgument[]) => (req: Request<import("express-serve-static-core").ParamsDictionary>, res: Response, next: NextFunction) => Promise<void>;
    validateBody: (...args: ValidationArgument[]) => any;
    validateParams: (...args: ValidationArgument[]) => any;
    validateQuery: (...args: ValidationArgument[]) => any;
    /**
     * Validates `limit`, and `offset` query args.
     */
    validateQueryPaging: () => any;
};
