"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var validatorEnums = {};
function registerValidatorEnum(nameOrObj, enumValues) {
    if (!nameOrObj) {
        return;
    }
    if (typeof nameOrObj === 'string') {
        validatorEnums[nameOrObj] = enumValues;
    }
    else if (typeof nameOrObj === 'object') {
        for (var name_1 in nameOrObj) {
            validatorEnums[name_1] = nameOrObj[name_1];
        }
    }
}
exports.registerValidatorEnum = registerValidatorEnum;
/**
 * @returns Validator enum values with a name previously passed to `registerValidatorEnum`.
 */
function getValidatorEnum(name) {
    return validatorEnums[name] || null;
}
exports.getValidatorEnum = getValidatorEnum;
