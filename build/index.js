"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __read = (this && this.__read) || function (o, n) {
    var m = typeof Symbol === "function" && o[Symbol.iterator];
    if (!m) return o;
    var i = m.call(o), r, ar = [], e;
    try {
        while ((n === void 0 || n-- > 0) && !(r = i.next()).done) ar.push(r.value);
    }
    catch (error) { e = { error: error }; }
    finally {
        try {
            if (r && !r.done && (m = i["return"])) m.call(i);
        }
        finally { if (e) throw e.error; }
    }
    return ar;
};
var __spread = (this && this.__spread) || function () {
    for (var ar = [], i = 0; i < arguments.length; i++) ar = ar.concat(__read(arguments[i]));
    return ar;
};
var __values = (this && this.__values) || function(o) {
    var s = typeof Symbol === "function" && Symbol.iterator, m = s && o[s], i = 0;
    if (m) return m.call(o);
    if (o && typeof o.length === "number") return {
        next: function () {
            if (o && i >= o.length) o = void 0;
            return { value: o && o[i++], done: !o };
        }
    };
    throw new TypeError(s ? "Object is not iterable." : "Symbol.iterator is not defined.");
};
Object.defineProperty(exports, "__esModule", { value: true });
var nested_property_1 = require("nested-property");
var validators_1 = require("./validators");
var enum_validator_1 = require("./enum_validator");
var enum_validator_2 = require("./enum_validator");
exports.registerValidatorEnum = enum_validator_2.registerValidatorEnum;
var isArray = Array.isArray;
var ValidationOpts = /** @class */ (function () {
    function ValidationOpts() {
    }
    return ValidationOpts;
}());
exports.ValidationOpts = ValidationOpts;
;
var ValidatorRule = /** @class */ (function (_super) {
    __extends(ValidatorRule, _super);
    function ValidatorRule() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return ValidatorRule;
}(ValidationOpts));
var Validator = /** @class */ (function (_super) {
    __extends(Validator, _super);
    function Validator() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    return Validator;
}(ValidatorRule));
exports.Validator = Validator;
var withValidationError = function (result, array) { return result === true ? array : __spread(array, [result]); };
var typeMap = {
    "s": "string",
    "b": "boolean",
    "n": "number",
    "i": "integer",
    "oid": "objectid",
    "d": "date",
    "date": "date",
    "fd": "datefuture",
    "fdate": "datefuture",
    "pd": "datepast",
    "pdate": "datepast",
    "@": "email",
    "@-": "emaildeep",
    "o": "object",
    "u": "absoluteurl",
    "du": "datauri",
    "swp": "swedishphone",
    "p": "phone",
    "fn": "fullname"
};
/**
 * Parses a property map as an object or array.
 * @param rawPath Original property path of the rule.
 * @param rule The rule itself.
 * @returns Parsed rule or nested rules in case the rule is a nested property map.
 */
function parsePropertyObjectMapRule(rawPath, rule) {
    var e_1, _a;
    var _b = parsePropertyPath(rawPath), optional = _b.optional, path = _b.path;
    if (isArray(rule)) {
        var subvalidators = [];
        try {
            for (var rule_1 = __values(rule), rule_1_1 = rule_1.next(); !rule_1_1.done; rule_1_1 = rule_1.next()) {
                var ruleElement = rule_1_1.value;
                if (typeof ruleElement === 'object') {
                    for (var path_1 in ruleElement) {
                        subvalidators = subvalidators.concat(parsePropertyObjectMapRule(path_1, ruleElement[path_1]));
                    }
                }
                else {
                    subvalidators = subvalidators.concat(parsePropertyObjectMapRule(rawPath, ruleElement));
                }
            }
        }
        catch (e_1_1) { e_1 = { error: e_1_1 }; }
        finally {
            try {
                if (rule_1_1 && !rule_1_1.done && (_a = rule_1.return)) _a.call(rule_1);
            }
            finally { if (e_1) throw e_1.error; }
        }
        return [{
                optional: optional,
                path: path,
                type: subvalidators
            }];
    }
    var isNestedObject = rule && typeof rule === "object";
    if (isNestedObject) {
        var subvalidators = [];
        for (var innerRawPath in rule) {
            subvalidators = subvalidators.concat(parsePropertyObjectMapRule(rawPath + "." + innerRawPath, rule[innerRawPath]));
        }
        return subvalidators;
    }
    var validator = parseValidatorRule(rule, rawPath);
    if (isArray(validator)) {
        return [{
                type: validator,
                optional: optional,
                path: path
            }];
    }
    else {
        return [__assign(__assign({}, validator), { optional: optional,
                path: path })];
    }
}
function parseValidators(opts) {
    var e_2, _a;
    var args = [];
    for (var _i = 1; _i < arguments.length; _i++) {
        args[_i - 1] = arguments[_i];
    }
    opts = opts || {};
    var validators = [];
    try {
        for (var args_1 = __values(args), args_1_1 = args_1.next(); !args_1_1.done; args_1_1 = args_1.next()) {
            var arg = args_1_1.value;
            if (!arg) {
                continue;
            }
            if (typeof arg === "string") {
                validators = validators.concat(parsePropertyMap(arg));
            }
            else if (typeof arg === "object") {
                for (var rawPath in arg) {
                    validators = validators.concat(parsePropertyObjectMapRule(rawPath, arg[rawPath]));
                }
            }
        }
    }
    catch (e_2_1) { e_2 = { error: e_2_1 }; }
    finally {
        try {
            if (args_1_1 && !args_1_1.done && (_a = args_1.return)) _a.call(args_1);
        }
        finally { if (e_2) throw e_2.error; }
    }
    var result = addOptions(addOptionalObjectValidators(replacePathPlaceholders(validators)), opts);
    return result;
}
exports.parseValidators = parseValidators;
function parsePropertyMap(str) {
    var properties = str.split(",");
    return properties.map(function (prop) { return parsePropertyString(prop); });
}
exports.parsePropertyMap = parsePropertyMap;
/**
 * Parses a validator string, such as `name=s[5;10]`.
 */
function parsePropertyString(element) {
    var regexMatch = element.match(/([.\w?$]+)=(.+)?/i);
    if ((regexMatch || []).length !== 3) {
        throw new Error("[parsePropertyString] invalid element syntax: " + element);
    }
    var _a = __read(regexMatch, 3), fullMatch = _a[0], rawPath = _a[1], value = _a[2];
    var _b = parsePropertyPath(rawPath), optional = _b.optional, path = _b.path;
    var validator = parseValidatorRule(value, rawPath);
    if (isArray(validator)) {
        return {
            optional: optional,
            path: path,
            type: validator
        };
    }
    return __assign({ optional: optional,
        path: path }, validator);
}
/**
 * Parses a property path, such as `name?` or `id`.
 */
function parsePropertyPath(path) {
    var optional = path.endsWith('?');
    return {
        optional: optional,
        path: optional ? path.substr(0, path.length - 1) : path
    };
}
/**
 * Parses a validator rule string, such as `=s[5;10]`.
 * @param rule Rule, such as `s[5;10]`
 * @param property Property name, such as `age`
 */
function parseValidatorRule(rule, property) {
    var hasSubvalidators = rule.startsWith('~');
    if (hasSubvalidators) {
        var subrules = rule.substr(1).split('|');
        return subrules.map(function (rule) { return parseValidatorRule(rule, property); });
    }
    var regexMatch = rule.match(/([@\w~-]+)(?:\[([\d.]+)?(?:;)?([\d.]+)?\])?(?:\&([<>])(\w+))?/);
    if (!(regexMatch || []).length) {
        throw new Error("[parseValidatorRule] invalid syntax: \"" + rule + "\" for property: \"" + property + "\"");
    }
    var _a = __read(regexMatch, 6), fullMatch = _a[0], type = _a[1], min = _a[2], max = _a[3], comparisonOperator = _a[4], comparisonPath = _a[5];
    //  fallback to `type` for user-defined types.
    var mappedType = typeMap[type] || type;
    if (min && isNaN(min)) {
        throw new Error("[parseValidatorRule] min is NaN: " + min + " for property: \"" + property + "\"");
    }
    if (max && isNaN(max)) {
        throw new Error("[parseValidatorRule] max is NaN: " + min + " for property: \"" + property + "\"");
    }
    var comparisonEligibleTypes = ['integer', 'number', 'date'];
    if ((comparisonOperator || comparisonPath) && !comparisonEligibleTypes.includes(mappedType)) {
        throw new Error("[parseValidatorRule] your type is ineligible for comparison validation: " + mappedType + ", eligible are: " + comparisonEligibleTypes.join(', '));
    }
    return {
        type: mappedType,
        min: min ? Number(min) : null,
        max: max ? Number(max) : null,
        compareWith: (comparisonOperator && comparisonPath) ? {
            operator: { '<': 'lessThan', '>': 'greaterThan' }[comparisonOperator],
            path: comparisonPath
        } : null
    };
}
function validateValue(value, validator) {
    return __awaiter(this, void 0, void 0, function () {
        var errors, subvalidators, subvalidateResult, validatorFn, hasEnum, result;
        return __generator(this, function (_a) {
            switch (_a.label) {
                case 0:
                    errors = [];
                    if (validator.optional && value == null) {
                        return [2 /*return*/, true];
                    }
                    if (!isArray(validator.type)) return [3 /*break*/, 2];
                    subvalidators = validator.type;
                    return [4 /*yield*/, validateObject(value, subvalidators.map(function (subvalidator) { return (__assign(__assign({}, validator), subvalidator)); }))];
                case 1:
                    subvalidateResult = _a.sent();
                    if (subvalidateResult !== true) {
                        errors = errors.concat(subvalidateResult);
                    }
                    return [3 /*break*/, 4];
                case 2:
                    validatorFn = validators_1.validators[validator.type];
                    if (typeof validatorFn !== "function") {
                        hasEnum = enum_validator_1.getValidatorEnum(validator.type) != null;
                        if (hasEnum) {
                            validatorFn = validators_1.validateEnum;
                        }
                        else {
                            throw new Error("[validateObject] unknown validator with type \"" + validator.type + "\"");
                        }
                    }
                    return [4 /*yield*/, validatorFn(value, validator.path, validator.min, validator.max, validator.type, validator)];
                case 3:
                    result = _a.sent();
                    if (result !== true) {
                        errors.push(result);
                    }
                    _a.label = 4;
                case 4: return [2 /*return*/, errors.length ? errors : true];
            }
        });
    });
}
/**
 * Validates an object using validators.
 */
function validateObject(obj, validators) {
    return __awaiter(this, void 0, void 0, function () {
        var errors, validators_2, validators_2_1, validator, memberOperatorIndex, parentObjectPath, parentObjectIsNull, value, isArrayOfSubvalidators, arrayValidateResult, _a, _b, subvalue, result, e_3_1, result, e_4_1;
        var e_4, _c, e_3, _d;
        return __generator(this, function (_e) {
            switch (_e.label) {
                case 0:
                    if (!isArray(validators) || !validators.length) {
                        return [2 /*return*/, true];
                    }
                    errors = [];
                    _e.label = 1;
                case 1:
                    _e.trys.push([1, 15, 16, 17]);
                    validators_2 = __values(validators), validators_2_1 = validators_2.next();
                    _e.label = 2;
                case 2:
                    if (!!validators_2_1.done) return [3 /*break*/, 14];
                    validator = validators_2_1.value;
                    if (validator.parentObjectIsOptional) {
                        memberOperatorIndex = validator.path.lastIndexOf('.');
                        if (memberOperatorIndex > 0) {
                            parentObjectPath = validator.path.substr(0, memberOperatorIndex);
                            parentObjectIsNull = nested_property_1.get(obj, parentObjectPath) == null;
                            if (parentObjectIsNull) {
                                return [3 /*break*/, 13];
                            }
                        }
                    }
                    value = nested_property_1.get(obj, validator.path);
                    isArrayOfSubvalidators = isArray(validator.type);
                    if (!isArrayOfSubvalidators) return [3 /*break*/, 11];
                    if (validator.optional && value == null) {
                        return [3 /*break*/, 13];
                    }
                    arrayValidateResult = validators_1.validateArray(value, validator.path, null, null, null, validator);
                    if (arrayValidateResult !== true) {
                        errors.push(arrayValidateResult);
                        return [3 /*break*/, 13];
                    }
                    _e.label = 3;
                case 3:
                    _e.trys.push([3, 8, 9, 10]);
                    _a = (e_3 = void 0, __values(value)), _b = _a.next();
                    _e.label = 4;
                case 4:
                    if (!!_b.done) return [3 /*break*/, 7];
                    subvalue = _b.value;
                    return [4 /*yield*/, validateValue(subvalue, validator)];
                case 5:
                    result = _e.sent();
                    if (result !== true) {
                        errors = errors.concat(result);
                    }
                    _e.label = 6;
                case 6:
                    _b = _a.next();
                    return [3 /*break*/, 4];
                case 7: return [3 /*break*/, 10];
                case 8:
                    e_3_1 = _e.sent();
                    e_3 = { error: e_3_1 };
                    return [3 /*break*/, 10];
                case 9:
                    try {
                        if (_b && !_b.done && (_d = _a.return)) _d.call(_a);
                    }
                    finally { if (e_3) throw e_3.error; }
                    return [7 /*endfinally*/];
                case 10: return [3 /*break*/, 13];
                case 11: return [4 /*yield*/, validateValue(value, validator)];
                case 12:
                    result = _e.sent();
                    if (result !== true) {
                        errors = errors.concat(result);
                    }
                    _e.label = 13;
                case 13:
                    validators_2_1 = validators_2.next();
                    return [3 /*break*/, 2];
                case 14: return [3 /*break*/, 17];
                case 15:
                    e_4_1 = _e.sent();
                    e_4 = { error: e_4_1 };
                    return [3 /*break*/, 17];
                case 16:
                    try {
                        if (validators_2_1 && !validators_2_1.done && (_c = validators_2.return)) _c.call(validators_2);
                    }
                    finally { if (e_4) throw e_4.error; }
                    return [7 /*endfinally*/];
                case 17:
                    errors = __spread(errors, validateComparisons(validators, obj));
                    return [2 /*return*/, errors.length ? errors : true];
            }
        });
    });
}
exports.validateObject = validateObject;
function pathHasPlaceholder(path) {
    return /\$/.test(path);
}
/**
 * Replaces path placeholders, i.e. `$`, with their previous subpath counterpart.
 */
function replacePathPlaceholders(originalValidators) {
    var mutated = __spread(originalValidators);
    for (var i = 1, n = mutated.length; i < n; i++) {
        var validator = mutated[i];
        if (isArray(validator.type)) {
            continue;
        }
        if (!pathHasPlaceholder(validator.path)) {
            continue;
        }
        var subpaths = validator.path.split('.');
        var indexOfPlaceholder = subpaths.indexOf('$');
        if (indexOfPlaceholder === -1) {
            continue;
        }
        var reference = void 0;
        for (var j = i - 1; j >= 0; j--) {
            var subvalidator = mutated[j];
            if (isArray(subvalidator)) {
                continue;
            }
            if (!pathHasPlaceholder(subvalidator.path)) {
                reference = subvalidator;
                break;
            }
        }
        if (!reference) {
            throw new Error("[replacePathPlaceholders] no reference for " + validator.path);
        }
        var referenceSubpaths = reference.path.split('.');
        subpaths[indexOfPlaceholder] = referenceSubpaths[indexOfPlaceholder];
        validator.path = subpaths.join('.');
    }
    return mutated;
}
/**
 * Adds missing object validators for objects which are optional.
 *
 * If the object is required then no additional validators is needed
 * as the object's fields are validated.
 */
function addOptionalObjectValidators(originalValidators) {
    var seenOptionalObjectPaths = {};
    var mutated = __spread(originalValidators);
    for (var i = 0, n = mutated.length; i < n; i++) {
        var validator = mutated[i];
        if (!validator) {
            continue;
        }
        var subpaths = validator.path.split('.');
        var isNestedProperty = subpaths.length > 1;
        if (!isNestedProperty) {
            continue;
        }
        var parentObjectPath = validator.path.substr(0, validator.path.lastIndexOf('.'));
        if (parentObjectPath.endsWith('?')) {
            parentObjectPath = parentObjectPath.substr(0, parentObjectPath.length - 1);
            mutated[i].path = parentObjectPath + "." + subpaths[subpaths.length - 1];
            mutated[i].parentObjectIsOptional = true;
            if (!seenOptionalObjectPaths[parentObjectPath]) {
                seenOptionalObjectPaths[parentObjectPath] = true;
                //  insert before the first nested element, which is this.
                mutated.splice(i, 0, {
                    optional: true,
                    path: parentObjectPath,
                    type: 'object'
                });
                i++;
                n++;
            }
        }
    }
    return mutated;
}
/**
 * Applies `ValidationOpts` to a set of validators.
 */
function addOptions(validators, opts) {
    return validators.map(function (validator) { return (__assign(__assign({}, validator), (opts || undefined))); });
}
/**
 * Validates comparison rules.
 * @param validators Validates to check for comparisons.
 * @param obj The value source, will be searched for referenced paths.
 * @returns Validation errors, or an empty array.
 */
function validateComparisons(validators, obj) {
    var e_5, _a;
    var isNumeric = function (type) { return type == 'integer' || type == 'number'; };
    var isDate = function (type) { return type == 'date' || type == 'datepast' || type == 'datefuture'; };
    var errors = [];
    var _loop_1 = function (compareWith, path, type) {
        if (!compareWith) {
            return "continue";
        }
        var referencedValidator = validators.find(function (_a) {
            var path = _a.path;
            return path == compareWith.path;
        });
        if (!referencedValidator) {
            throw new Error("Unknown validator with path " + compareWith.path);
        }
        var actualValue = nested_property_1.get(obj, path);
        var referencedValue = nested_property_1.get(obj, compareWith.path);
        if (isNumeric(type)) {
            if (!isNumeric(referencedValidator.type)) {
                errors.push("`" + referencedValidator.path + "` must be numeric as it is referenced by `" + path + "`");
                return "continue";
            }
            errors = withValidationError(validators_1.metaValidators.validateNumberByComparison(Number(actualValue), Number(referencedValue), path, compareWith.path, compareWith.operator), errors);
        }
        else if (isDate) {
            if (!isDate(referencedValidator.type)) {
                errors.push("`" + referencedValidator.path + "` must be a date as it is referenced by `" + path + "`");
                return "continue";
            }
            errors = withValidationError(validators_1.metaValidators.validateDateByComparison(actualValue, referencedValue, path, compareWith.path, compareWith.operator), errors);
        }
    };
    try {
        for (var validators_3 = __values(validators), validators_3_1 = validators_3.next(); !validators_3_1.done; validators_3_1 = validators_3.next()) {
            var _b = validators_3_1.value, compareWith = _b.compareWith, path = _b.path, type = _b.type;
            _loop_1(compareWith, path, type);
        }
    }
    catch (e_5_1) { e_5 = { error: e_5_1 }; }
    finally {
        try {
            if (validators_3_1 && !validators_3_1.done && (_a = validators_3.return)) _a.call(validators_3);
        }
        finally { if (e_5) throw e_5.error; }
    }
    return errors;
}
exports.koa = {
    /**
     * Validate a koa request using your own selector.
     *
     * If you are passing a multipart form but would prefer to a JSON body,
     * use thie function to select and validate the JSON body part.
     *
     * @param selector Selects object to validate
     * @param args Validation arguments
     * @returns Validation middleware
     */
    validate: function (selector, opts) {
        var args = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            args[_i - 2] = arguments[_i];
        }
        var validators = parseValidators.apply(void 0, __spread([opts], args));
        return function (ctx, next) {
            return __awaiter(this, void 0, void 0, function () {
                var result;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, validateObject(selector(ctx), validators)];
                        case 1:
                            result = _a.sent();
                            if (result !== true) {
                                ctx.status = 400;
                                ctx.body = {
                                    error: 'validationError',
                                    validation: result
                                };
                                return [2 /*return*/];
                            }
                            return [4 /*yield*/, next()];
                        case 2:
                            _a.sent();
                            return [2 /*return*/];
                    }
                });
            });
        };
    },
    validateBody: function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return exports.koa.validate.apply(exports.koa, __spread([function (ctx) { return ctx.request.body; }, null], args));
    },
    validateParams: function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return exports.koa.validate.apply(exports.koa, __spread([function (ctx) { return ctx.params; }, null], args));
    },
    validateQuery: function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return exports.koa.validate.apply(exports.koa, __spread([function (ctx) { return ctx.query; }, {
                validateBooleanStrings: true,
                validateNumericStrings: true
            }], args));
    },
    /**
     * Validates `limit`, and `offset` query args.
     */
    validateQueryPaging: function () {
        return exports.koa.validateQuery('limit?=i[0;]', 'offset?=i[0;]');
    }
};
exports.express = {
    validate: function (selector, opts) {
        var args = [];
        for (var _i = 2; _i < arguments.length; _i++) {
            args[_i - 2] = arguments[_i];
        }
        var validators = parseValidators.apply(void 0, __spread([opts], args));
        return function (req, res, next) {
            return __awaiter(this, void 0, void 0, function () {
                var result;
                return __generator(this, function (_a) {
                    switch (_a.label) {
                        case 0: return [4 /*yield*/, validateObject(selector(req, res), validators)];
                        case 1:
                            result = _a.sent();
                            if (result !== true) {
                                res.status(400).json({
                                    error: 'validationError',
                                    validation: result
                                });
                                return [2 /*return*/, next(new Error('validation failed'))];
                            }
                            return [2 /*return*/, next()];
                    }
                });
            });
        };
    },
    validateBody: function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return exports.express.validate.apply(exports.express, __spread([function (req) { return req.body; }, null], args));
    },
    validateParams: function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return exports.express.validate.apply(exports.express, __spread([function (req) { return req.params; }, null], args));
    },
    validateQuery: function () {
        var args = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            args[_i] = arguments[_i];
        }
        return exports.express.validate.apply(exports.express, __spread([function (req) { return req.query; }, {
                validateNumericStrings: true,
                validateBooleanStrings: true
            }], args));
    },
    /**
     * Validates `limit`, and `offset` query args.
     */
    validateQueryPaging: function () {
        return exports.express.validateQuery('limit?=i[0;]', 'offset?=i[0;]');
    }
};
