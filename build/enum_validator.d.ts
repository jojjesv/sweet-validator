/**
 * Registers a validator enum so that it can be referenced in validator objects.
 *
 * For convenience, pass an object with a key having the same name as it's value.
 * @example You can pass `{ myEnum }`, which is equivalent to `registerValidatorEnum('myEnum', myEnum)`
 */
export declare function registerValidatorEnum(obj: {
    [name: string]: any[];
}): void;
/**
 * Registers a validator enum so that it can be referenced in validator objects.
 */
export declare function registerValidatorEnum(name: string, enumValues: any[]): void;
/**
 * @returns Validator enum values with a name previously passed to `registerValidatorEnum`.
 */
export declare function getValidatorEnum(name: string): any[];
