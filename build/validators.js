"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
var bson_1 = require("bson");
var validator_1 = __importDefault(require("validator"));
var enum_validator_1 = require("./enum_validator");
var is_absolute_url_1 = __importDefault(require("is-absolute-url"));
var is_data_uri_1 = __importDefault(require("is-data-uri"));
var EmailDeepValidator = require('email-deep-validator');
var validateString = function (src, name, min, max) {
    var validates = typeof src === "string";
    var useMin = typeof min === "number";
    var useMax = typeof max === "number";
    if (validates) {
        if (useMin) {
            validates = validates && src.length >= min;
        }
        if (useMax) {
            validates = validates && src.length <= max;
        }
    }
    if (!validates) {
        if (useMin && useMax) {
            if (min == max) {
                return "`" + name + "` must be a string of exactly " + max + " characters";
            }
            return "`" + name + "` must be a string within " + min + " and " + max + " characters";
        }
        else if (!useMin && !useMax) {
            return "`" + name + "` must be a string";
        }
        else if (useMin) {
            return "`" + name + "` must be a string of atleast " + min + " characters";
        }
        else if (useMax) {
            return "`" + name + "` must be a string of at most " + max + " characters";
        }
    }
    return true;
};
var validateNumber = function (src, name, min, max, type, opts) {
    if (opts.validateNumericStrings === true && !isNaN(src)) {
        src = Number(src);
    }
    var validates = typeof src === "number";
    var useMin = typeof min === "number";
    var useMax = typeof max === "number";
    if (validates) {
        if (useMin) {
            validates = validates && src >= min;
        }
        if (useMax) {
            validates = validates && src <= max;
        }
    }
    if (!validates) {
        if (useMin && useMax) {
            return "`" + name + "` must be numeric within " + min + " and " + max;
        }
        else if (!useMin && !useMax) {
            return "`" + name + "` must be numeric";
        }
        else if (useMin) {
            return "`" + name + "` must be numeric of atleast " + min;
        }
        else if (useMax) {
            return "`" + name + "` must be numeric of at most " + max;
        }
    }
    return true;
};
var validateNumberByComparison = function (value, referencedValue, name, referencedName, operator) {
    switch (operator) {
        case 'lessThan':
            if (value >= referencedValue) {
                return "`" + name + "` must be less than `" + referencedName + "`";
            }
            break;
        case 'greaterThan':
            if (value <= referencedValue) {
                return "`" + name + "` must be greater than `" + referencedName + "`";
            }
            break;
    }
    return true;
};
var validateInteger = function (src, name, min, max) {
    var validates = typeof src === "number";
    var useMin = typeof min === "number";
    var useMax = typeof max === "number";
    var hasDecimals = validates && src % 1 !== 0;
    validates = !hasDecimals;
    if (validates) {
        if (useMin) {
            validates = validates && src >= min;
        }
        if (useMax) {
            validates = validates && src <= max;
        }
    }
    if (!validates) {
        if (!useMin && !useMax || hasDecimals) {
            return "`" + name + "` must be an integer";
        }
        else if (useMin && useMax) {
            return "`" + name + "` must be an integer within " + min + " and " + max;
        }
        else if (useMin) {
            return "`" + name + "` must be an integer of atleast " + min;
        }
        else if (useMax) {
            return "`" + name + "` must be an integer of at most " + max;
        }
    }
    return true;
};
var booleanLike = ['true', 'false', 'yes', 'no'];
var validateBoolean = function (src, name, min, max, type, opts) {
    if (opts.validateBooleanStrings === true && booleanLike.indexOf(String(src)) != -1) {
        return true;
    }
    if (typeof src !== "boolean") {
        return "`" + name + "` must be a boolean";
    }
    return true;
};
var validateDate = function (src, name) {
    if (typeof src !== "string" || isNaN(new Date(src).getTime())) {
        return "`" + name + "` must be a date string";
    }
    return true;
};
var validateDateByComparison = function (value, referencedValue, name, referencedName, operator) {
    var p = new Date(value).getTime();
    var q = new Date(referencedValue).getTime();
    switch (operator) {
        case 'lessThan':
            if (p > q) {
                return "`" + name + "` must be before `" + referencedName + "`";
            }
            break;
        case 'greaterThan':
            if (p < q) {
                return "`" + name + "` must be after `" + referencedName + "`";
            }
            break;
    }
    return true;
};
var validateFutureDate = function (src, name, min, max, type, opts) {
    var dateValidation = validateDate(src, name, min, max, type, opts);
    if (dateValidation !== true) {
        return dateValidation;
    }
    var now = new Date();
    if (new Date(src) <= now) {
        return "`" + name + "` must be a future date string";
    }
    return true;
};
var validatePastDate = function (src, name, min, max, type, opts) {
    var dateValidation = validateDate(src, name, min, max, type, opts);
    if (dateValidation !== true) {
        return dateValidation;
    }
    var now = new Date();
    if (new Date(src) >= now) {
        return "`" + name + "` must be a past date string";
    }
    return true;
};
var validateObjectId = function (src, name) {
    if (!bson_1.ObjectID.isValid(src)) {
        return "`" + name + "` must be a valid object ID";
    }
    return true;
};
var validateEmail = function (src, name) {
    if (typeof src != 'string' || !validator_1.default.isEmail(src)) {
        return "`" + name + "` must be a valid e-mail address";
    }
    return true;
};
var validateEmailDeep = function (src, name, min, max, type, opts) { return __awaiter(void 0, void 0, void 0, function () {
    var validator, _a, wellFormed, validDomain, validMailbox, e_1;
    return __generator(this, function (_b) {
        switch (_b.label) {
            case 0:
                validator = new EmailDeepValidator({
                    timeout: 1250
                });
                _b.label = 1;
            case 1:
                _b.trys.push([1, 3, , 4]);
                return [4 /*yield*/, validator.verify(src)];
            case 2:
                _a = _b.sent(), wellFormed = _a.wellFormed, validDomain = _a.validDomain, validMailbox = _a.validMailbox;
                return [3 /*break*/, 4];
            case 3:
                e_1 = _b.sent();
                console.error("validateEmailDeep failed, falling back to validateEmail", e_1);
                return [2 /*return*/, validateEmail(src, name, null, null, null, opts)];
            case 4:
                if (!wellFormed || !validDomain) {
                    return [2 /*return*/, "`" + name + "` must be a valid, operating e-mail address"];
                }
                return [2 /*return*/, true];
        }
    });
}); };
exports.validateEnum = function (src, name, min, max, enumName, validator) {
    if (validator.optional && src == null) {
        return true;
    }
    var enumValues = enum_validator_1.getValidatorEnum(enumName);
    if (enumValues.indexOf(src) === -1) {
        return "`" + name + "` must be one of: " + enumValues.join(',');
    }
    return true;
};
exports.validateArray = function (src, name) {
    if (!Array.isArray(src)) {
        return "`" + name + "` must be an array";
    }
    return true;
};
exports.validateObject = function (src, name) {
    if (!(src && typeof src === "object" && !Array.isArray(src))) {
        return "`" + name + "` must be an object";
    }
    return true;
};
exports.validateAbsoluteUrl = function (src, name) {
    if (!src || !is_absolute_url_1.default(src)) {
        return "`" + name + "` must be an absolute URL";
    }
    return true;
};
exports.validateDataUri = function (src, name) {
    if (!src || !is_data_uri_1.default(src)) {
        return "`" + name + "` must be a data URI";
    }
    return true;
};
exports.validatePhoneAnyLocale = function (src, name) {
    if (!src || !validator_1.default.isMobilePhone(src, 'any')) {
        return "`" + name + "` must be a valid phone number";
    }
    return true;
};
exports.validatePhoneSwedish = function (src, name) {
    if (!src || !validator_1.default.isMobilePhone(src, 'sv-SE')) {
        return "`" + name + "` must be a valid Swedish phone number";
    }
    return true;
};
exports.validateFullName = function (src, name) {
    if (src) {
        src = src.normalize("NFD").replace(/[\u0300-\u036f]/g, '');
    }
    if (!src || !/^[a-z]+ ([a-z]+ )?[a-z]+$/i.test(src)) {
        return "`" + name + "` must be a valid full name";
    }
    return true;
};
exports.validators = {
    'string': validateString,
    'boolean': validateBoolean,
    'number': validateNumber,
    'integer': validateInteger,
    'object': exports.validateObject,
    'objectid': validateObjectId,
    'date': validateDate,
    'datefuture': validateFutureDate,
    'datepast': validatePastDate,
    'email': validateEmail,
    'emaildeep': validateEmailDeep,
    'absoluteurl': exports.validateAbsoluteUrl,
    'datauri': exports.validateDataUri,
    'phone': exports.validatePhoneAnyLocale,
    'swedishphone': exports.validatePhoneSwedish,
    'fullname': exports.validateFullName
};
exports.metaValidators = {
    validateDateByComparison: validateDateByComparison,
    validateNumberByComparison: validateNumberByComparison
};
