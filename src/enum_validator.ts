const validatorEnums: {
  [name: string]: any[]
} = {}

/**
 * Registers a validator enum so that it can be referenced in validator objects.
 * 
 * For convenience, pass an object with a key having the same name as it's value.
 * @example You can pass `{ myEnum }`, which is equivalent to `registerValidatorEnum('myEnum', myEnum)`
 */
export function registerValidatorEnum(obj: { [name: string]: any[] }): void;

/**
 * Registers a validator enum so that it can be referenced in validator objects.
 */
export function registerValidatorEnum(name: string, enumValues: any[]): void;

export function registerValidatorEnum(nameOrObj: any, enumValues?: any[]) {
  if (!nameOrObj) {
    return;
  }

  if (typeof nameOrObj === 'string') {
    validatorEnums[nameOrObj] = enumValues;
  } else if (typeof nameOrObj === 'object') {
    for (let name in nameOrObj) {
      validatorEnums[name] = nameOrObj[name];
    }
  }
}

/**
 * @returns Validator enum values with a name previously passed to `registerValidatorEnum`.
 */
export function getValidatorEnum(name: string): any[] {
  return validatorEnums[name] || null;
}