import { BaseContext, Context, DefaultContext, ParameterizedContext } from "koa";
import { get as property } from 'nested-property';
import { ValidationResult, validators as validatorFns, validateEnum, validateArray, metaValidators } from "./validators";
import { Request, Response, NextFunction } from "express";
import { getValidatorEnum } from "./enum_validator";

export { registerValidatorEnum } from './enum_validator';

const { isArray } = Array;

type PropertyStringMap = string;

export class ValidationOpts {
  /**
   * Whether to validate numeric-like strings as numerics, such as `"42"`.
   * @default false
   */
  validateNumericStrings?: boolean;

  /**
   * Whether to validate boolean-like strings as boolean, such as `"false"`.
   * @default false
   */
  validateBooleanStrings?: boolean;
};

class ValidatorRule extends ValidationOpts {
  type: ValidatorType | ValidatorRule[];
  min?: number;
  max?: number;
  compareWith?: {
    operator: ComparisonValidationOperator;
    path: string;
  };
}

export class Validator extends ValidatorRule {
  optional: boolean;
  path: string;

  parentObjectIsOptional?: boolean;
}

export type ComparisonValidationOperator = 'lessThan' | 'greaterThan';

type ValidationArgument = PropertyStringMap | { [propName: string]: ValidationArgument | ValidationArgument[] };

export type ValidatorType = 'string' | 'boolean' | 'number' | 'integer' | 'object' | 'objectid' | 'date'
  | 'datefuture' | 'datepast' | 'email' | 'emaildeep' | 'absoluteurl' | 'datauri' | 'phone' | 'swedishphone'
  | 'fullname';

const withValidationError = <T>(result: ValidationResult, array: T[]) => result === true ? array : [...array, result];

const typeMap: { [macro: string]: ValidatorType } = {
  "s": "string",
  "b": "boolean",
  "n": "number",
  "i": "integer",
  "oid": "objectid",
  "d": "date",
  "date": "date",
  "fd": "datefuture",
  "fdate": "datefuture",
  "pd": "datepast",
  "pdate": "datepast",
  "@": "email",
  "@-": "emaildeep",
  "o": "object",
  "u": "absoluteurl",
  "du": "datauri",
  "swp": "swedishphone",
  "p": "phone",
  "fn": "fullname"
};

/**
 * Parses a property map as an object or array.
 * @param rawPath Original property path of the rule.
 * @param rule The rule itself.
 * @returns Parsed rule or nested rules in case the rule is a nested property map.
 */
function parsePropertyObjectMapRule(rawPath: string, rule: any): Validator[] {
  const { optional, path } = parsePropertyPath(rawPath);

  if (isArray(rule)) {
    let subvalidators: Validator[] = [];

    for (let ruleElement of rule) {
      if (typeof ruleElement === 'object') {
        for (let path in ruleElement) {
          subvalidators = subvalidators.concat(
            parsePropertyObjectMapRule(path, ruleElement[path])
          );
        }
      } else {
        subvalidators = subvalidators.concat(
          parsePropertyObjectMapRule(rawPath, ruleElement)
        );
      }
    }

    return [{
      optional,
      path,
      type: subvalidators
    }];
  }

  const isNestedObject = rule && typeof rule === "object";
  if (isNestedObject) {
    let subvalidators: Validator[] = [];

    for (let innerRawPath in rule) {
      subvalidators = subvalidators.concat(
        parsePropertyObjectMapRule(`${rawPath}.${innerRawPath}`, rule[innerRawPath])
      );
    }

    return subvalidators;
  }

  const validator = parseValidatorRule(rule, rawPath);

  if (isArray(validator)) {
    return [{
      type: validator,
      optional,
      path
    }];
  } else {
    return [{
      ...validator,
      optional,
      path
    }];
  }
}

export function parseValidators(opts: ValidationOpts, ...args: ValidationArgument[]): Validator[] {
  opts = opts || {};

  let validators: Validator[] = [];

  for (let arg of args) {
    if (!arg) {
      continue;
    }

    if (typeof arg === "string") {
      validators = validators.concat(parsePropertyMap(arg));
    } else if (typeof arg === "object") {
      for (let rawPath in arg as any) {
        validators = validators.concat(parsePropertyObjectMapRule(rawPath, arg[rawPath]));
      }
    }
  }

  const result = addOptions(
    addOptionalObjectValidators(
      replacePathPlaceholders(validators)
    ), opts
  );

  return result;
}

export function parsePropertyMap(str: PropertyStringMap): Validator[] {
  const properties = str.split(",");
  return properties.map(prop => parsePropertyString(prop));
}

/**
 * Parses a validator string, such as `name=s[5;10]`.
 */
function parsePropertyString(element: string): Validator {
  const regexMatch = element.match(
    /([.\w?$]+)=(.+)?/i
  );

  if ((regexMatch || []).length !== 3) {
    throw new Error(`[parsePropertyString] invalid element syntax: ${element}`)
  }

  const [fullMatch, rawPath, value] = regexMatch;

  const { optional, path } = parsePropertyPath(rawPath);
  const validator = parseValidatorRule(value, rawPath);

  if (isArray(validator)) {
    return {
      optional,
      path,
      type: validator
    };
  }

  return {
    optional,
    path,
    ...validator
  };
}

/**
 * Parses a property path, such as `name?` or `id`.
 */
function parsePropertyPath(path: string) {
  const optional = path.endsWith('?');

  return {
    optional,
    path: optional ? path.substr(0, path.length - 1) : path
  };
}

/**
 * Parses a validator rule string, such as `=s[5;10]`.
 * @param rule Rule, such as `s[5;10]`
 * @param property Property name, such as `age`
 */
function parseValidatorRule(rule: string, property: string): ValidatorRule | ValidatorRule[] {
  const hasSubvalidators = rule.startsWith('~');

  if (hasSubvalidators) {
    const subrules = rule.substr(1).split('|');
    return subrules.map(rule => parseValidatorRule(rule, property) as Validator);
  }

  const regexMatch = rule.match(
    /([@\w~-]+)(?:\[([\d.]+)?(?:;)?([\d.]+)?\])?(?:\&([<>])(\w+))?/
  );

  if (!(regexMatch || []).length) {
    throw new Error(`[parseValidatorRule] invalid syntax: "${rule}" for property: "${property}"`);
  }

  const [fullMatch, type, min, max, comparisonOperator, comparisonPath] = regexMatch;

  //  fallback to `type` for user-defined types.
  const mappedType = typeMap[type] || type;

  if (min && isNaN(min as any)) {
    throw new Error(`[parseValidatorRule] min is NaN: ${min} for property: "${property}"`)
  }

  if (max && isNaN(max as any)) {
    throw new Error(`[parseValidatorRule] max is NaN: ${min} for property: "${property}"`)
  }

  const comparisonEligibleTypes = ['integer', 'number', 'date'];

  if ((comparisonOperator || comparisonPath) && !comparisonEligibleTypes.includes(mappedType)) {
    throw new Error(`[parseValidatorRule] your type is ineligible for comparison validation: ${mappedType}, eligible are: ${comparisonEligibleTypes.join(', ')}`)
  }

  return {
    type: mappedType as ValidatorType,
    min: min ? Number(min) : null,
    max: max ? Number(max) : null,
    compareWith: (comparisonOperator && comparisonPath) ? {
      operator: { '<': 'lessThan', '>': 'greaterThan' }[comparisonOperator],
      path: comparisonPath
    } : null
  };
}

async function validateValue(value: any, validator: Validator): Promise<true | ValidationResult[]> {
  let errors: ValidationResult[] = [];

  if (validator.optional && value == null) {
    return true;
  }

  if (isArray(validator.type)) {
    //  Validate an array.
    const subvalidators = validator.type;
    const subvalidateResult = await validateObject(value, subvalidators.map(subvalidator => ({
      ...validator,
      ...subvalidator
    }) as Validator));

    if (subvalidateResult !== true) {
      errors = errors.concat(subvalidateResult);
    }
  } else {
    let validatorFn = validatorFns[validator.type];

    if (typeof validatorFn !== "function") {
      const hasEnum = getValidatorEnum(validator.type) != null;
      if (hasEnum) {
        validatorFn = validateEnum;
      } else {
        throw new Error(`[validateObject] unknown validator with type "${validator.type}"`);
      }
    }

    const result = await validatorFn(value, validator.path, validator.min, validator.max, validator.type, validator);

    if (result !== true) {
      errors.push(result);
    }
  }

  return errors.length ? errors : true;
}

/**
 * Validates an object using validators.
 */
export async function validateObject(obj: any, validators: Validator[]): Promise<true | ValidationResult[]> {
  if (!isArray(validators) || !validators.length) {
    return true;
  }

  let errors: ValidationResult[] = [];

  for (let validator of validators) {
    if (validator.parentObjectIsOptional) {
      const memberOperatorIndex = validator.path.lastIndexOf('.');

      if (memberOperatorIndex > 0) {
        const parentObjectPath = validator.path.substr(0, memberOperatorIndex);
        const parentObjectIsNull = property(obj, parentObjectPath) == null;

        if (parentObjectIsNull) {
          continue;
        }
      }
    }

    const value = property(obj, validator.path);

    const isArrayOfSubvalidators = isArray(validator.type);

    if (isArrayOfSubvalidators) {
      if (validator.optional && value == null) {
        continue;
      }

      const arrayValidateResult = validateArray(value, validator.path, null, null, null, validator);
      if (arrayValidateResult !== true) {
        errors.push(arrayValidateResult as ValidationResult);
        continue;
      }

      for (let subvalue of value as any[]) {
        const result = await validateValue(subvalue, validator);
        if (result !== true) {
          errors = errors.concat(result);
        }
      }
    } else {
      const result = await validateValue(value, validator);
      if (result !== true) {
        errors = errors.concat(result);
      }
    }
  }

  errors = [...errors, ...validateComparisons(validators, obj)];

  return errors.length ? errors : true;
}

function pathHasPlaceholder(path: string) {
  return /\$/.test(path);
}

/**
 * Replaces path placeholders, i.e. `$`, with their previous subpath counterpart.
 */
function replacePathPlaceholders(originalValidators: Validator[]): Validator[] {
  const mutated = [...originalValidators];

  for (let i = 1, n = mutated.length; i < n; i++) {
    const validator = mutated[i];

    if (isArray(validator.type)) {
      continue;
    }

    if (!pathHasPlaceholder(validator.path)) {
      continue;
    }

    const subpaths = validator.path.split('.');
    const indexOfPlaceholder = subpaths.indexOf('$');

    if (indexOfPlaceholder === -1) {
      continue;
    }

    let reference: Validator;

    for (let j = i - 1; j >= 0; j--) {
      const subvalidator = mutated[j];
      if (isArray(subvalidator)) {
        continue;
      }

      if (!pathHasPlaceholder(subvalidator.path)) {
        reference = subvalidator;
        break;
      }
    }

    if (!reference) {
      throw new Error(`[replacePathPlaceholders] no reference for ${validator.path}`);
    }

    const referenceSubpaths = reference.path.split('.');
    subpaths[indexOfPlaceholder] = referenceSubpaths[indexOfPlaceholder];

    validator.path = subpaths.join('.');
  }

  return mutated;
}

/**
 * Adds missing object validators for objects which are optional.
 * 
 * If the object is required then no additional validators is needed
 * as the object's fields are validated.
 */
function addOptionalObjectValidators(originalValidators: Validator[]): Validator[] {
  const seenOptionalObjectPaths: {
    [objectPath: string]: true
  } = {};

  let mutated = [...originalValidators];

  for (let i = 0, n = mutated.length; i < n; i++) {
    const validator = mutated[i];
    if (!validator) {
      continue;
    }

    const subpaths = validator.path.split('.');

    const isNestedProperty = subpaths.length > 1;

    if (!isNestedProperty) {
      continue;
    }

    let parentObjectPath = validator.path.substr(0, validator.path.lastIndexOf('.'));

    if (parentObjectPath.endsWith('?')) {
      parentObjectPath = parentObjectPath.substr(0, parentObjectPath.length - 1);

      mutated[i].path = `${parentObjectPath}.${subpaths[subpaths.length - 1]}`;
      mutated[i].parentObjectIsOptional = true;

      if (!seenOptionalObjectPaths[parentObjectPath]) {
        seenOptionalObjectPaths[parentObjectPath] = true;

        //  insert before the first nested element, which is this.
        mutated.splice(i, 0, {
          optional: true,
          path: parentObjectPath,
          type: 'object'
        });
        i++;
        n++;
      }
    }
  }

  return mutated;
}

/**
 * Applies `ValidationOpts` to a set of validators.
 */
function addOptions(validators: Validator[], opts: ValidationOpts) {
  return validators.map(validator => ({
    ...validator,
    ...(opts || undefined)
  }));
}

/**
 * Validates comparison rules.
 * @param validators Validates to check for comparisons.
 * @param obj The value source, will be searched for referenced paths.
 * @returns Validation errors, or an empty array.
 */
function validateComparisons(validators: Validator[], obj: any) {
  const isNumeric = (type: ValidatorType) => type == 'integer' || type == 'number';
  const isDate = (type: ValidatorType) => type == 'date' || type == 'datepast' || type == 'datefuture';

  let errors: string[] = [];

  for (let { compareWith, path, type } of validators) {
    if (!compareWith) {
      continue;
    }

    const referencedValidator = validators.find(({ path }) => path == compareWith.path);
    if (!referencedValidator) {
      throw new Error(`Unknown validator with path ${compareWith.path}`);
    }

    const actualValue = property(obj, path);
    const referencedValue = property(obj, compareWith.path);

    if (isNumeric(type as ValidatorType)) {
      if (!isNumeric(referencedValidator.type as ValidatorType)) {
        errors.push(`\`${referencedValidator.path}\` must be numeric as it is referenced by \`${path}\``);
        continue;
      }

      errors = withValidationError(
        metaValidators.validateNumberByComparison(Number(actualValue), Number(referencedValue), path, compareWith.path, compareWith.operator),
        errors
      );
    } else if (isDate) {
      if (!isDate(referencedValidator.type as ValidatorType)) {
        errors.push(`\`${referencedValidator.path}\` must be a date as it is referenced by \`${path}\``);
        continue;
      }

      errors = withValidationError(
        metaValidators.validateDateByComparison(actualValue, referencedValue, path, compareWith.path, compareWith.operator),
        errors
      );
    }
  }

  return errors;
}

export const koa = {
  /**
   * Validate a koa request using your own selector.
   * 
   * If you are passing a multipart form but would prefer to a JSON body,
   * use thie function to select and validate the JSON body part.
   * 
   * @param selector Selects object to validate
   * @param args Validation arguments
   * @returns Validation middleware
   */
  validate: function (selector: (context: ParameterizedContext) => any, opts: ValidationOpts, ...args: ValidationArgument[]): any {
    const validators = parseValidators(opts, ...args);

    return async function (ctx: ParameterizedContext<any, any>, next: () => Promise<any>) {
      const result = await validateObject(selector(ctx), validators);
      if (result !== true) {
        ctx.status = 400;
        ctx.body = {
          error: 'validationError',
          validation: result
        };
        return;
      }

      await next();
    };
  },

  validateBody: function (...args: ValidationArgument[]): any {
    return koa.validate(ctx => (ctx.request as any).body, null, ...args);
  },

  validateParams: function (...args: ValidationArgument[]): any {
    return koa.validate(ctx => ctx.params, null, ...args);
  },

  validateQuery: function (...args: ValidationArgument[]): any {
    return koa.validate(ctx => ctx.query, {
      validateBooleanStrings: true,
      validateNumericStrings: true
    }, ...args);
  },

  /**
   * Validates `limit`, and `offset` query args.
   */
  validateQueryPaging: function (): any {
    return koa.validateQuery('limit?=i[0;]', 'offset?=i[0;]');
  }
}

export const express = {
  validate: function (selector: (req: Request, res: Response) => any, opts: ValidationOpts, ...args: ValidationArgument[]) {
    const validators = parseValidators(opts, ...args);

    return async function (req: Request, res: Response, next: NextFunction) {
      const result = await validateObject(selector(req, res), validators);
      if (result !== true) {
        res.status(400).json({
          error: 'validationError',
          validation: result
        });
        return next(new Error('validation failed'));
      }

      return next();
    };
  },

  validateBody: function (...args: ValidationArgument[]): any {
    return express.validate(req => req.body, null, ...args);
  },

  validateParams: function (...args: ValidationArgument[]): any {
    return express.validate(req => req.params, null, ...args);
  },

  validateQuery: function (...args: ValidationArgument[]): any {
    return express.validate(req => req.query, {
      validateNumericStrings: true,
      validateBooleanStrings: true
    }, ...args);
  },

  /**
   * Validates `limit`, and `offset` query args.
   */
  validateQueryPaging: function (): any {
    return express.validateQuery('limit?=i[0;]', 'offset?=i[0;]');
  }
}