import { ObjectID } from "bson";
import validator from 'validator';
import { getValidatorEnum } from "./enum_validator";
import isAbsoluteUrl from 'is-absolute-url'
import isDataUri from 'is-data-uri';
import { ComparisonValidationOperator, Validator as ValidatorMeta } from './';
const EmailDeepValidator = require('email-deep-validator');

type Validator = (value: any, name: string, min: number | null, max: number | null, type: string, validator: ValidatorMeta) => ValidationResult | Promise<ValidationResult>;

type ComparisonValidator<T> = (value: T, referencedValue: T, name: string, referencedName: string, operator: ComparisonValidationOperator) => ValidationResult;

export type ValidationResult = true | ValidationError;
type ValidationError = string;

const validateString: Validator = (src: any, name: string, min, max) => {
  let validates = typeof src === "string";

  const useMin = typeof min === "number";
  const useMax = typeof max === "number";

  if (validates) {
    if (useMin) {
      validates = validates && src.length >= min;
    }
    if (useMax) {
      validates = validates && src.length <= max;
    }
  }

  if (!validates) {
    if (useMin && useMax) {
      if (min == max) {
        return `\`${name}\` must be a string of exactly ${max} characters`;
      }

      return `\`${name}\` must be a string within ${min} and ${max} characters`;
    } else if (!useMin && !useMax) {
      return `\`${name}\` must be a string`;
    } else if (useMin) {
      return `\`${name}\` must be a string of atleast ${min} characters`;
    } else if (useMax) {
      return `\`${name}\` must be a string of at most ${max} characters`;
    }
  }

  return true;
}

const validateNumber: Validator = (src: any, name: string, min, max, type, opts) => {
  if (opts.validateNumericStrings === true && !isNaN(src)) {
    src = Number(src);
  }

  let validates = typeof src === "number";

  const useMin = typeof min === "number";
  const useMax = typeof max === "number";

  if (validates) {
    if (useMin) {
      validates = validates && src >= min;
    }
    if (useMax) {
      validates = validates && src <= max;
    }
  }

  if (!validates) {
    if (useMin && useMax) {
      return `\`${name}\` must be numeric within ${min} and ${max}`;
    } else if (!useMin && !useMax) {
      return `\`${name}\` must be numeric`;
    } else if (useMin) {
      return `\`${name}\` must be numeric of atleast ${min}`;
    } else if (useMax) {
      return `\`${name}\` must be numeric of at most ${max}`;
    }
  }

  return true;
}

const validateNumberByComparison: ComparisonValidator<Number> = (value, referencedValue, name, referencedName, operator) => {
  switch (operator) {
    case 'lessThan':
      if (value >= referencedValue) {
        return `\`${name}\` must be less than \`${referencedName}\``;
      }
      break;
    case 'greaterThan':
      if (value <= referencedValue) {
        return `\`${name}\` must be greater than \`${referencedName}\``;
      }
      break;
  }

  return true;
}

const validateInteger: Validator = (src: any, name: string, min, max) => {
  let validates = typeof src === "number";

  const useMin = typeof min === "number";
  const useMax = typeof max === "number";

  const hasDecimals = validates && src % 1 !== 0;

  validates = !hasDecimals;

  if (validates) {
    if (useMin) {
      validates = validates && src >= min;
    }
    if (useMax) {
      validates = validates && src <= max;
    }
  }

  if (!validates) {
    if (!useMin && !useMax || hasDecimals) {
      return `\`${name}\` must be an integer`;
    } else if (useMin && useMax) {
      return `\`${name}\` must be an integer within ${min} and ${max}`;
    } else if (useMin) {
      return `\`${name}\` must be an integer of atleast ${min}`;
    } else if (useMax) {
      return `\`${name}\` must be an integer of at most ${max}`;
    }
  }

  return true;
}

const booleanLike = ['true', 'false', 'yes', 'no'];
const validateBoolean: Validator = (src: any, name: string, min, max, type, opts) => {
  if (opts.validateBooleanStrings === true && booleanLike.indexOf(String(src)) != -1) {
    return true;
  }

  if (typeof src !== "boolean") {
    return `\`${name}\` must be a boolean`;
  }

  return true;
}

const validateDate: Validator = (src: any, name: string) => {
  if (typeof src !== "string" || isNaN(new Date(src).getTime())) {
    return `\`${name}\` must be a date string`;
  }

  return true;
}

const validateDateByComparison: ComparisonValidator<Date> = (value, referencedValue, name, referencedName, operator) => {
  const p = new Date(value).getTime();
  const q = new Date(referencedValue).getTime();

  switch (operator) {
    case 'lessThan':
      if (p > q) {
        return `\`${name}\` must be before \`${referencedName}\``;
      }
      break;
    case 'greaterThan':
      if (p < q) {
        return `\`${name}\` must be after \`${referencedName}\``;
      }
      break;
  }

  return true;
}

const validateFutureDate: Validator = (src: any, name: string, min, max, type, opts) => {
  const dateValidation = validateDate(src, name, min, max, type, opts);
  if (dateValidation !== true) {
    return dateValidation;
  }

  const now = new Date();
  if (new Date(src) <= now) {
    return `\`${name}\` must be a future date string`;
  }

  return true;
}

const validatePastDate: Validator = (src: any, name: string, min, max, type, opts) => {
  const dateValidation = validateDate(src, name, min, max, type, opts);
  if (dateValidation !== true) {
    return dateValidation;
  }

  const now = new Date();
  if (new Date(src) >= now) {
    return `\`${name}\` must be a past date string`;
  }

  return true;
}

const validateObjectId: Validator = (src: any, name: string) => {
  if (!ObjectID.isValid(src)) {
    return `\`${name}\` must be a valid object ID`;
  }

  return true;
}

const validateEmail: Validator = (src: any, name: string) => {
  if (typeof src != 'string' || !validator.isEmail(src)) {
    return `\`${name}\` must be a valid e-mail address`;
  }

  return true;
}

const validateEmailDeep: Validator = async (src: any, name: string, min, max, type, opts) => {
  const validator = new EmailDeepValidator({
    timeout: 1250
  });
  try {
    var { wellFormed, validDomain, validMailbox } = await validator.verify(src);
  } catch (e) {
    console.error(`validateEmailDeep failed, falling back to validateEmail`, e);
    return validateEmail(src, name, null, null, null, opts);
  }

  if (!wellFormed || !validDomain) {
    return `\`${name}\` must be a valid, operating e-mail address`;
  }

  return true;
}

export const validateEnum: Validator = (src: any, name: string, min, max, enumName, validator) => {
  if (validator.optional && src == null) {
    return true;
  }

  const enumValues = getValidatorEnum(enumName);

  if (enumValues.indexOf(src) === -1) {
    return `\`${name}\` must be one of: ${enumValues.join(',')}`
  }

  return true;
}

export const validateArray: Validator = (src: any, name: string) => {
  if (!Array.isArray(src)) {
    return `\`${name}\` must be an array`;
  }

  return true;
}

export const validateObject: Validator = (src: any, name: string) => {
  if (!(src && typeof src === "object" && !Array.isArray(src))) {
    return `\`${name}\` must be an object`;
  }

  return true;
}

export const validateAbsoluteUrl: Validator = (src: any, name: string) => {
  if (!src || !isAbsoluteUrl(src)) {
    return `\`${name}\` must be an absolute URL`;
  }

  return true;
}

export const validateDataUri: Validator = (src: any, name: string) => {
  if (!src || !isDataUri(src)) {
    return `\`${name}\` must be a data URI`;
  }

  return true;
}

export const validatePhoneAnyLocale: Validator = (src: any, name: string) => {
  if (!src || !validator.isMobilePhone(src, 'any')) {
    return `\`${name}\` must be a valid phone number`;
  }

  return true;
}

export const validatePhoneSwedish: Validator = (src: any, name: string) => {
  if (!src || !validator.isMobilePhone(src, 'sv-SE')) {
    return `\`${name}\` must be a valid Swedish phone number`;
  }

  return true;
}

export const validateFullName: Validator = (src: any, name: string) => {
  if (src) {
    src = src.normalize("NFD").replace(/[\u0300-\u036f]/g, '');
  }
  if (!src || !/^[a-z]+ ([a-z]+ )?[a-z]+$/i.test(src)) {
    return `\`${name}\` must be a valid full name`;
  }

  return true;
}

export const validators = {
  'string': validateString,
  'boolean': validateBoolean,
  'number': validateNumber,
  'integer': validateInteger,
  'object': validateObject,
  'objectid': validateObjectId,
  'date': validateDate,
  'datefuture': validateFutureDate,
  'datepast': validatePastDate,
  'email': validateEmail,
  'emaildeep': validateEmailDeep,
  'absoluteurl': validateAbsoluteUrl,
  'datauri': validateDataUri,
  'phone': validatePhoneAnyLocale,
  'swedishphone': validatePhoneSwedish,
  'fullname': validateFullName
};

export const metaValidators = {
  validateDateByComparison,
  validateNumberByComparison
}